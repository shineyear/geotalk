<?php

$CUR = 0;
$TOKENS = null;
$UID = 1;
$ALL = 0;
$REDO = 0;

function &get_token()
{
    global $CUR;
    global $TOKENS;
    global $ALL;

    if ($CUR >= count($TOKENS))
    {
        $CUR = 0; 
    }

    $token = &$TOKENS[$CUR];
    $CUR++;

    if ($token['status'] != 0)
    {
	$ALL += 1;
	if ($ALL >= count($TOKENS))
	{
	    print "all token status error\n";
            exit();	
	}

        return get_token();
    }


    if (array_key_exists('send', $token))
    {
	if (time() - $token['time'] <= 15*60 && $token['send'] >= 15)
	{
            $token['sleep'] = 1;
	}
	else
	{
	    if ($token['send'] >= 15)
	    {
	        $token['send'] = 0; 
                $token['time'] = time();
	    }

            $token['send'] += 1;
            $token['sleep'] = 0;
	    $token['find'] = 0;
	}

    }
    else
    {
        $token['send'] = 1;
        $token['sleep'] = 0;
        $token['time'] = time();
	$token['find'] = 0;
    }

    if (array_key_exists('sleep', $token))
    {
        if ($token['sleep'] == 1) 
	{
	    $token['find'] += 1;

	    if ($token['find'] >= 2)
	    {
	        sleep(60); 
	        #$token['find'] = 0;
	    }

	    return get_token();
	}
    }

    return $token;
}


function email($show, $domain, $from, $to, $name, $id, $money)
{

    $subject_en = "Payment Confirmed From $show";
    $message_en = "Dear Customer: \n\nThanks for using $domain service , we already got your payment $id $$money for $name , we will process your request in 48 hours, \n\nCheers.";

    $subject_ja = "$show で確認支払い";
    $message_ja = "お客様各位: \n\n$domain サービスを使用していただきありがとうございます , 我々はすでにあなたの支払を得た $id $$money のために $name , 私たちは48時間以内にあなたの要求を処理します , \n\n乾杯.";

    $subject_fr = "Paiement Confirmé De $show";
    $message_fr = "Cher client: \n\nMerci pour avoir utilisé le service de $domain , nous avons déjà obtenu votre paiement $id $$money pour $name , nous traiterons votre demande en 48 heures , \n\nSanté.";

    $subject_ko = "$show 에서 결제 확인";
    $message_ko = "친애하는 고객: \n\n$domain 서비스를 이용해 주셔서 감사합니다 , 우리는 이미 당신의 지불을 얻은 $id $$money 에 $name , 우리는 48 시간에 귀하의 요청을 처리합니다 , \n\n건배.";

    $subject_de = "Von der bestätigten Zahlung $show";
    $message_de = "Sehr geehrter Kunde: \n\nDanke für die Benutzung $domain Service , wir bereits Ihre Zahlung erhielt $id $$money für $name , Wir werden Ihre Anfrage innerhalb von 48 Stunden zu verarbeiten , \n\nProst.";

    $subject_hk = "付款確認從$show";
    $message_hk = "尊敬的客戶: \n\n感謝您使用 $domain 服務 , 我們已經得到您的付款 $id $$money 為 $name , 我們會處理您的要求在48小時內 , \n\n乾杯.";

    $subject_cn = "付款确认从 $show";
    $message_cn = "尊敬的客户: \n\n感谢您使用 $domain 的服务，我们已经得到您的付款 $id $$money 为 $name , 我们会处理您的要求在48小时内, \n\n干杯.";

    $subject_pt = "Pagamento Confirmado De $show";
    $message_pt = "Prezado Cliente: \n\nObrigado por usar o serviço $domain , já temos o seu pagamento $id $$money para $name , vamos processar o seu pedido em 48 horas , \n\nSaúde.";

    $subject_it = "Pagamento Confermato Da $show";
    $message_it = "Gentile Cliente: \n\nGrazie per aver utilizzato il servizio $domain , abbiamo già ottenuto il vostro pagamento $id $$money per $name , noi elaborare la tua richiesta in 48 ore , \n\nCin cin.";

    $subject_es = "Pago Confirmado Desde $show";
    $message_es = "Estimado cliente: \n\n¡Gracias por utilizar el servicio $domain , ya tenemos su pago $id $$money para $name , procesaremos su petición en 48 horas , \n\n¡Salud.";

    $subject_el = "Πληρωμή Επιβεβαιώθηκε Από $show";
    $message_el = "Αγαπητέ πελάτη: \n\nΕυχαριστώ για τη χρήση των υπηρεσιών $domain , έχουμε ήδη πήρα την πληρωμή σας $id $$money για $name , θα επεξεργαστούμε το αίτημά σας εντός 48 ωρών , \n\nστην υγειά σας.";

    $subject_ru = "Оплата Подтвержденные От $show";
    $message_ru = "Уважаемый покупатель: \n\nСпасибо за использование $domain обслуживание , мы уже получили вашу компенсацию $id $$money для $name , мы обработаем ваш запрос в течение 48 часов , \n\nВаше здоровье.";

    $subject_ar = "تأكيد الدفع من $show";
    $message_ar = "عزيزي العميل: \n\nشكرا لاستخدام خدمة $domain , نحن بالفعل حصلنا على الدفع الخاصة بك $id $$money إلى $name , وسوف نقوم بمعالجة طلبك في 48 ساعة , \n\nفي صحتك. ";

    $subject_th = "การชำระเงินได้รับการยืนยันจาก $show";
    $message_th = "เรียนท่านลูกค้า: \n\nขอขอบคุณที่ใช้บริการ $domain , แล้วที่เราได้ชำระเงินของคุณ $id $$money เพื่อ $name , เราจะดำเนินการตามคำขอของคุณใน 48 ชั่วโมง , \n\nไชโย.";

    $headers = "From:" . $from;
    echo $name;
    $language = strtolower(substr($name, -3, 2));
    echo $language;

    switch ($language) {
	case "en":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_en) . "?=", $message_en, $headers);
	    break;

	case "ja":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_ja) . "?=", $message_ja, $headers);
	    break;

	case "fr":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_fr) . "?=", $message_fr, $headers);
	    break;

	case "ko":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_ko) . "?=", $message_ko, $headers);
	    break;

	case "de":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_de) . "?=", $message_de, $headers);
	    break;

	case "hk":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_hk) . "?=", $message_hk, $headers);
	    break;

	case "tw":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_hk) . "?=", $message_hk, $headers);
	    break;

	case "cn":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_cn) . "?=", $message_cn, $headers);
	    break;

	case "pt":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_pt) . "?=", $message_pt, $headers);
	    break;

	case "it":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_it) . "?=", $message_it, $headers);
	    break;

	case "es":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_es) . "?=", $message_es, $headers);
	    break;

	case "el":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_el) . "?=", $message_el, $headers);
	    break;

	case "ru":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_ru) . "?=", $message_ru, $headers);
	    break;

	case "ar":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_ar) . "?=", $message_ar, $headers);
	    break;
	
	case "th":
            mail($to, "=?UTF-8?B?" . base64_encode($subject_th) . "?=", $message_th, $headers);
	    break;
	    
	default:
            mail($to, $subject_en, $message_en, $headers);
	    break;
    }

    echo "Confirm Mail Sent $language\n";
}

function call_list($show, $domain, $from, $to, $name, $id, $money, $info)
{

    $array = explode('|', $info);
    $size = count($array);

    if ($size != 5)
    {
       echo "list size error"; 
       return;
    }

    $geo = explode(",", $array[0]);

    if (count($geo) != 4)
    {
       echo "list geo error"; 
       return;
    }

    print_r($geo);

    $bottom_max = $geo[0];
    $left_max = $geo[1];
    $top_max = $geo[2];
    $right_max = $geo[3];

    $city = $array[1];
    $language = $array[2];
    $trans = $array[3];
    $domain = $array[4];

    $sql = "SELECT DISTINCT name FROM user WHERE lat <= ".$top_max." AND lat >= ".$bottom_max." AND lng <= ".$right_max." AND lng >= ".$left_max." ORDER BY last DESC LIMIT 100000";

    $mysqli = new mysqli("mysql.geottt.com", "geotttdb", "1q!2w@3e#", "gttt_".strtolower($city));

    if (mysqli_connect_errno()) 
    {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $results = $mysqli->query($sql);

    $list = $results->fetch_all();

    $results->free();

    $mysqli->close();

    $headers = "From:" . $from;

    $new_arr = array();
    foreach($list as $key => $value) 
    {
	array_push($new_arr, $value[0]);
    }
    echo "count: ".count($new_arr)."\n";

    mail($to, "=?UTF-8?B?" . base64_encode($name) . "?=", implode("\n", $new_arr), $headers);

    echo "list Mail Sent\n";
}

function call_test($account, $message)
{

    $token = &get_token();


    $cmd = 'php send.php "'.$account.'" "'.$message.'" "'.$token['consumer_key'].'" "'.$token['consumer_secret'].'" "'.$token['access_token'].'" "'.$token['access_token_secret'].'"';
    $res = shell_exec($cmd);
    $obj = json_decode($res);

    if (array_key_exists('errors', $obj))
    {
        $code = $obj->{'errors'}[0]->{'code'};

	if ($code == 64)
	{
            $mysqli = new mysqli("mysql.geottt.com", "geotttdb", "1q!2w@3e#", "geotalk");

            $mysqli->query("UPDATE token SET status = 1 WHERE id = ".$token['id']);

            if (!$mysqli->commit()) 
            {
                print("Transaction commit failed\n");
                exit();
            }

            $mysqli->close();

	    $token['status'] = 1;

            print "removed token ".$token['id']."\n";	
	    call_test($account, $message);
	}
	else if ($code == 187)
	{
	    call_test($account, $message.' '.rand());
	}
	else if ($code == 88)
	{
	
	    $token['sleep'] = 1;
	    $token['send'] = 15;
	    call_test($account, $message.' '.rand());
	}
    }

}

$admin_sql = "SELECT admin.id, admin.username, admin.domain, admin.show, admin.email, admin.phone FROM geotalk.admin WHERE id = $UID and status = 0";
$token_sql = "SELECT id, status, consumer_key, consumer_secret, access_token, access_token_secret, uid FROM token WHERE uid = $UID and status = 0";
$sql = "SELECT id, content, status, time FROM geotalk.order WHERE status = 0 and domain = ";

if ($REDO != 0) 
{
    $sql = "SELECT id, content, status, time FROM geotalk.order WHERE id = $REDO";
}

$update_sql = "UPDATE geotalk.order SET status = 1, last = now() WHERE id = ";
$update2_sql = "UPDATE geotalk.order SET status = 2, last = now() WHERE id = ";

$mysqli = new mysqli("mysql.geottt.com", "geotttdb", "1q!2w@3e#", "geotalk");


if (mysqli_connect_errno()) 
{
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$results = $mysqli->query($admin_sql);

$admin = $results->fetch_row();

$show = $admin[3];
#$show = "GTTT";
$domain = $admin[2];
#$domain = "geottt.com";
$from = $admin[4];
#$from = "admin@geottt.com";

$results->free();

$results = $mysqli->query($token_sql);

$TOKENS = $results->fetch_all(MYSQLI_ASSOC);

$results->free();

$results = $mysqli->query($sql."'$domain'");

$jobs = $results->fetch_all(MYSQLI_ASSOC);

$results->free();

$job_num = count($jobs);

print "job: ".$job_num."\n";

if ($job_num == 0)
{
//run sender account self update
}

foreach ($jobs as $row)
{

    print "jobid: ".$row['id']."\n";
    parse_str($row["content"], $output);
    print_r($output);

    if ($output['payment_status'] != 'Completed')
    {
	print "not complete payment";
	continue;
    }

    $mysqli->query($update_sql.$row['id']);

    if (!$mysqli->commit()) 
    {
        print("Transaction commit failed\n");
        exit();
    }

    email($show, $domain, $from, $output['payer_email'], $output['item_name'], $output['receipt_id'], $output['mc_gross']);
}


$mysqli->close();

foreach ($jobs as $row)
{
    parse_str($row["content"], $output);
    #continue;
    
    if ($output['payment_status'] != 'Completed')
    {
	print "not complete payment";
	continue;
    }


    switch ($output['item_number'])
    {
        case 3:
	    call_test($output['option_selection2'], str_replace('"', '\"', $output['option_selection1']));
	    break;
	case 2:
	    print "case 2";
	    break;
	case 1:
	    call_list($show, $domain, $from, $output['payer_email'], $output['item_name'], $output['receipt_id'], $output['mc_gross'], $output['option_selection1']);
	    break;
	default:
	    print "case default";
	    break;
    }

    $mysqli = new mysqli("mysql.geottt.com", "geotttdb", "1q!2w@3e#", "geotalk");

    $mysqli->query($update2_sql.$row['id']);

    if (!$mysqli->commit()) 
    {
        print("Transaction 2 commit failed\n");
	exit();
    }

    $mysqli->close();

    print "job finish ".$row['id']."\n";
}



?>
