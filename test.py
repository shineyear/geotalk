import re
import json
import signal
import sys
import traceback
#import threading
import urllib
import httplib
import socket
import urlparse
import time
import datetime
#import sqlite3
import memcache
import MySQLdb
import numpy

from twython import Twython
from twython import TwythonStreamer
from SocketServer import ThreadingMixIn
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

TOKENS = None
TOKENS_LEN = 0
CUR_TOKEN = 0
CONN = None
COUNT = 50
LOCATION = '%LOCATION%'
DBFILE = '%DBFILE%'
CITY = '%CITY%'
TKFILE = './token.txt'
max_top = None
max_bottom = None
max_left = None
max_right = None
#a = [[0 for x in range(rows)] for y in range(columns)]
x_y_size = 50
geo_arr = [[0 for x in xrange(x_y_size)] for y in xrange(x_y_size)] 
fresh = 600
cur_time = 0

def get_token(next=False):

    global CUR_TOKEN

    if next:
	if CUR_TOKEN >= TOKENS_LEN - 1:
	    CUR_TOKEN = 0
	else:
	    CUR_TOKEN += 1

    return TOKENS['token'][CITY][CUR_TOKEN]


def reload_token(signum, frame):

    global TOKENS
    global TOKENS_LEN

    try:

        json_data = open(TKFILE)
        TOKENS = json.load(json_data)
        json_data.close()


        for item in TOKENS['token'][CITY]:
            print item['name']

	TOKENS_LEN = len(TOKENS['token'][CITY])

    except:
	traceback.print_exc(file=sys.stdout)
	pass

def get_conn(token):

    try:
        twitter = Twython(token['consumer_key'], token['consumer_secret'], token['access_token'], token['access_token_secret'])
	return twitter
    except:
	traceback.print_exc(file=sys.stdout)
	pass

    return None

def send_message(message, id):

    global CONN

    ret = None

    print "message:"
    print message

    try:
	#ret = CONN.retweet(id=id, text=message)
	print "begin"
	ret = CONN.update_status(status=message, in_reply_to_status_id=id)
	print "end"
    except:
	try:
    	    print "begin2"
            token = get_token(True)
            CONN = get_conn(token)
	    ret = CONN.update_status(status=message, in_reply_to_status_id=id)
    	    print "end2"
	except:
    	    #ret = CONN.retweet(id=id, text=message)
    	    traceback.print_exc(file=sys.stdout)

	pass

    print "ret:"
    print ret

#def store_to_db(id, uid, name, lat, lng):
def store_to_db(id, uid, name, lat, lng, rname, text, create, image, media, type):
    try:

	float_lat = float(lat)
	float_lng = float(lng)

        conn = MySQLdb.connect(host="mysql.geottt.com", # your host, usually localhost
                               user="geotttdb", # your username
                               passwd="1q!2w@3e#", # your password
                               db="gttt_%s" % CITY.lower()) # name of the data base
                               #db="geotalk") # name of the data base
        #conn = sqlite3.connect(DBFILE)
        c = conn.cursor()

        sql = 'SELECT id, uid, lat, lng FROM user WHERE lat = %.3f AND lng = %.3f AND uid = %s LIMIT 1' % (float_lat, float_lng, uid)

	c.execute(sql)
        ret = c.fetchone()

	if ret is None or ret == () or ret == []:
	    print "insert new"
            sql = 'INSERT INTO user (name, lat, lng, last, uid, time, lats, lngs, city, rname, text, `create`, `image`, `media`, `type`) VALUES (\'%s\', %.3f, %.3f, %s, %s, %d, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')' % (name, float_lat, float_lng, id, uid, time.time(), lat, lng, CITY, rname, text, create, image, media, type)

	    c.execute(sql)

	else:
	    print "update one"
            sql = 'UPDATE user SET name = \'%s\', last = %s, time = %d, lats = \'%s\', lngs = \'%s\', city = \'%s\', rname = \'%s\', text = \'%s\', `create` = \'%s\', `image` = \'%s\', `media` = \'%s\', `type` = \'%s\' WHERE id = %d' % (name, id, time.time(), lat, lng, CITY, rname, text, create, image, media, type, ret[0])

	    c.execute(sql)

	conn.commit()

    except:
    	traceback.print_exc(file=sys.stdout)
    finally:
        conn.close()

def geohash(border, lat, lng, debug=True):

    try:

        if debug:
            print border

        geo_piece = border['geo_piece']
        left_bottom = border['southwest'].split(',')
        right_top = border['northeast'].split(',')

        right_top_x = float(right_top[1])
        left_bottom_y = float(left_bottom[0])

        left_bottom_x = float(left_bottom[1])
        right_top_y = float(right_top[0])

        x_length = abs(right_top_x - left_bottom_x) / geo_piece
        y_length = abs(left_bottom_y - right_top_y) / geo_piece

        if debug:
            print x_length, y_length

        #x,y both from right bottom, from 0 to geo_piece - 1
	#change to from left top need - left top
        #x = abs(right_top_x - float(lng)) / x_length
        #y = abs(left_bottom_y - float(lat)) / y_length

        x = abs(float(lng) - left_bottom_x) / x_length
        y = abs(left_bottom_y - float(lat)) / y_length

        #x = abs(float(lng)) / x_length
        #y = abs(float(lat)) / y_length

        if debug:
            print x, y

        string_x = str(x)
        string_y = str(y)

        return (string_x[:string_x.find('.')], string_y[:string_y.find('.')])

    except:
        if debug:
            traceback.print_exc(file=config.sys.stdout)
        pass

    return None


def set_to_mem(data, x, y):

    global COUNT
    global geo_arr
    global cur_time

    key = '%s_talk_%d' % (CITY, COUNT)
    value = {"city": CITY}
    geo_arr[y][x] += 1

    if isinstance(key, unicode):
        key = key.encode('utf8')


    if 'geo' in data:
	if 'coordinates' in data['geo']:
	    value['lat'] = data['geo']['coordinates'][0]
	    value['lng'] = data['geo']['coordinates'][1]

    if 'entities' in data:
	if 'media' in data['entities']:
	    value['media'] = data['entities']['media'][0]['media_url']
	    value['type'] = data['entities']['media'][0]['type']

    if 'text' in data:
	value['text'] = data['text']

    if 'user' in data:
	if 'profile_image_url' in data['user']:
	    value['image'] = data['user']['profile_image_url']

	if 'screen_name' in data['user']:
	    value['name'] = data['user']['screen_name']

	if 'name' in data['user']:
	    value['rname'] = data['user']['name']

	if 'id_str' in data['user']:
	    value['uid'] = data['user']['id_str']

    if 'id_str' in data:
	value['id'] = data['id_str']

    if 'created_at' in data:
	value['created_at'] = data['created_at']


    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    mc.set(key, json.dumps(value))

    now = time.time()

    if now - cur_time > fresh:

        cur_time = now

        #print geo_arr
        mc.set('%s_hot' % CITY, json.dumps(geo_arr))

        for i in x_y_size:
            for j in x_y_size:
                geo_arr[i][j] = 0

    COUNT -= 1

    if COUNT <= 0:
        COUNT = 50

def signal_handler(signal, frame):
    print 'You pressed Ctrl+C!'

class MyStreamer(TwythonStreamer):
    def on_success(self, data):

	id = None
	uid = None
	name = None
	rname = None
	lat = None
	lng = None
	text = None
	create = None
	image = None
	media = None
	type = None

	#result = re.sub(r'^RT|@([A-Za-z0-9_]+:?)', '', data['text'])
	#result = re.sub(r'https?:([A-Za-z0-9_/?:&\.]+)', '', result)
	try:

            if 'id_str' in data:
	        id = data['id_str']

	    if 'text' in data:
                text = data['text'].encode('utf-8').replace("'", " ")

	    if 'created_at' in data:
		create =  data['created_at']

	    if 'user' in data:

                if 'id_str' in data:
	            uid = data['user']['id_str']

	        if 'screen_name' in data['user']:
	            name = data['user']['screen_name'].encode('utf-8').replace("'", " ")

	        if 'name' in data['user']:
	            rname = data['user']['name'].encode('utf-8').replace("'", " ")

	        if 'profile_image_url' in data['user']:
	            image = data['user']['profile_image_url']

	    if 'geo' in data:
	        if data['geo'] is not None and 'coordinates' in data['geo']:
	            lat = data['geo']['coordinates'][0]
	            lng = data['geo']['coordinates'][1]

            if 'entities' in data:
	        if 'media' in data['entities']:
	            media = data['entities']['media'][0]['media_url']
	            type = data['entities']['media'][0]['type']


	    if id is None or uid is None or name is None or lat is None or lng is None or text is None or create is None:
	        return

            #print max_bottom, max_top, max_left, max_right
            #print lat, lng
            if max_bottom <= float(lat) <= max_top and max_left <= float(lng) <= max_right:

		string = 'city: %s, id: %s, uid: %s, time: %s' % (CITY, id, uid, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                try:
                    (x, y) = geohash({'southwest': '%s,%s' % (max_bottom, max_left), 'northeast': '%s,%s' % (max_top, max_right), 'geo_piece': x_y_size}, lat, lng, False)
	            print string, x, y
                    sys.stdout.flush()

	            set_to_mem(data, int(x), int(y))
                except:
                    pass


                store_to_db(id, uid, name, lat, lng, rname, text, create, image, media, type)
                #store_to_db(id, uid, name, lat, lng)
	        #send_message(string[:139], "372977017190227968")
	except:

	    traceback.print_exc(file=sys.stdout)
	    pass

    def on_error(self, status_code, data):
	print status_code
	if status_code == 420 or status_code == '420':
	    time.sleep(60)



def main():
    global max_bottom
    global max_left
    global max_top
    global max_right
    global CONN

    reload_token(None, None)
    token = get_token()
    CONN = get_conn(token)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGHUP, reload_token)
    stream = MyStreamer(token['consumer_key'], token['consumer_secret'], token['access_token'], token['access_token_secret'])

    
    arr = LOCATION.split(',')


    max_left = float(arr[0])
    max_bottom = float(arr[1])
    max_right = float(arr[2])
    max_top = float(arr[3])
    
    while True:
        try:
	    print "start stream"
            stream.statuses.filter(locations=LOCATION)
        except:
	    traceback.print_exc(file=sys.stdout)
            pass
    

if __name__ == "__main__":
    main()


