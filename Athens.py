import re
import json
import signal
import sys
import traceback
#import threading
import urllib
import httplib
import socket
import urlparse
import time
import datetime
#import sqlite3
import memcache
import MySQLdb

from twython import Twython
from twython import TwythonStreamer
from SocketServer import ThreadingMixIn
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

TOKENS = None
TOKENS_LEN = 0
CUR_TOKEN = 0
CONN = None
COUNT = 50
LOCATION = '23.6869862,37.9488181,23.7896925,38.0328563'
DBFILE = '/home/geotttsh/geotalk/geotalk_Athens'
CITY = 'Athens'
TKFILE = './token.txt'
max_top = None
max_bottom = None
max_left = None
max_right = None

def get_token(next=False):

    global CUR_TOKEN

    if next:
	if CUR_TOKEN >= TOKENS_LEN - 1:
	    CUR_TOKEN = 0
	else:
	    CUR_TOKEN += 1

    return TOKENS['token'][CITY][CUR_TOKEN]


def reload_token(signum, frame):

    global TOKENS
    global TOKENS_LEN

    try:

        json_data = open(TKFILE)
        TOKENS = json.load(json_data)
        json_data.close()


        for item in TOKENS['token'][CITY]:
            print item['name']

	TOKENS_LEN = len(TOKENS['token'][CITY])

    except:
	traceback.print_exc(file=sys.stdout)
	pass

def get_conn(token):

    try:
        twitter = Twython(token['consumer_key'], token['consumer_secret'], token['access_token'], token['access_token_secret'])
	return twitter
    except:
	traceback.print_exc(file=sys.stdout)
	pass

    return None

def send_message(message, id):

    global CONN

    ret = None

    print "message:"
    print message

    try:
	#ret = CONN.retweet(id=id, text=message)
	print "begin"
	ret = CONN.update_status(status=message, in_reply_to_status_id=id)
	print "end"
    except:
	try:
    	    print "begin2"
            token = get_token(True)
            CONN = get_conn(token)
	    ret = CONN.update_status(status=message, in_reply_to_status_id=id)
    	    print "end2"
	except:
    	    #ret = CONN.retweet(id=id, text=message)
    	    traceback.print_exc(file=sys.stdout)

	pass

    print "ret:"
    print ret

def store_to_db(id, uid, name, lat, lng):
    try:

	float_lat = float(lat)
	float_lng = float(lng)

        conn = MySQLdb.connect(host="mysql.geottt.com", # your host, usually localhost
                               user="geotttdb", # your username
                               passwd="1q!2w@3e#", # your password
                               db="geotalk") # name of the data base
        #conn = sqlite3.connect(DBFILE)
        c = conn.cursor()

        sql = 'SELECT id, uid, lat, lng FROM user WHERE lat = %.3f AND lng = %.3f AND uid = %s LIMIT 1' % (float_lat, float_lng, uid)

	c.execute(sql)
        ret = c.fetchone()

	if ret is None or ret == () or ret == []:
	    print "insert new"
            sql = 'INSERT INTO user (name, lat, lng, last, uid, time, lats, lngs, city) VALUES (\'%s\', %.3f, %.3f, %s, %s, %d, \'%s\', \'%s\', \'%s\')' % (name, float_lat, float_lng, id, uid, time.time(), lat, lng, CITY)

	    c.execute(sql)

	else:
	    print "update one"
            sql = 'UPDATE user SET name = \'%s\', last = %s, time = %d, lats = \'%s\', lngs = \'%s\', city = \'%s\' WHERE id = %d' % (name, id, time.time(), lat, lng, CITY, ret[0])

	    c.execute(sql)

	conn.commit()

    except:
    	traceback.print_exc(file=sys.stdout)
    finally:
        conn.close()



def set_to_mem(data):

    global COUNT

    key = '%s_talk_%d' % (CITY, COUNT)
    value = {"city": CITY}

    if isinstance(key, unicode):
        key = key.encode('utf8')


    if 'geo' in data:
	if 'coordinates' in data['geo']:
	    value['lat'] = data['geo']['coordinates'][0]
	    value['lng'] = data['geo']['coordinates'][1]

    if 'entities' in data:
	if 'media' in data['entities']:
	    value['media'] = data['entities']['media'][0]['media_url']
	    value['type'] = data['entities']['media'][0]['type']

    if 'text' in data:
	value['text'] = data['text']

    if 'user' in data:
	if 'profile_image_url' in data['user']:
	    value['image'] = data['user']['profile_image_url']

	if 'screen_name' in data['user']:
	    value['name'] = data['user']['screen_name']

	if 'name' in data['user']:
	    value['rname'] = data['user']['name']

	if 'id_str' in data['user']:
	    value['uid'] = data['user']['id_str']

    if 'id_str' in data:
	value['id'] = data['id_str']


    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    mc.set(key, json.dumps(value))

    COUNT -= 1

    if COUNT <= 0:
        COUNT = 50

def signal_handler(signal, frame):
    print 'You pressed Ctrl+C!'

class MyStreamer(TwythonStreamer):
    def on_success(self, data):

	id = None
	uid = None
	name = None
	lat = None
	lng = None
	text = None

	#result = re.sub(r'^RT|@([A-Za-z0-9_]+:?)', '', data['text'])
	#result = re.sub(r'https?:([A-Za-z0-9_/?:&\.]+)', '', result)
	try:

            if 'id_str' in data:
	        id = data['id_str']

	    if 'text' in data:
                text = data['text']

	    if 'user' in data:
	        uid = data['user']['id_str']
	        name = data['user']['screen_name']

	    if 'geo' in data:
	        if data['geo'] is not None and 'coordinates' in data['geo']:
	            lat = data['geo']['coordinates'][0]
	            lng = data['geo']['coordinates'][1]

	    if id is None or uid is None or name is None or lat is None or lng is None or text is None:
	        return

            #print max_bottom, max_top, max_left, max_right
            #print lat, lng
            if max_bottom <= float(lat) <= max_top and max_left <= float(lng) <= max_right:

		string = 'city: %s, id: %s, uid: %s, name: %s, time: %s' % (CITY, id, uid, name, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                try:
	            print string
                    sys.stdout.flush()
	            set_to_mem(data)
                except:
                    pass


                store_to_db(id, uid, name, lat, lng)
	        #send_message(string[:139], "372977017190227968")
	except:

	    traceback.print_exc(file=sys.stdout)
	    pass

    def on_error(self, status_code, data):
	print status_code
	if status_code == 420 or status_code == '420':
	    time.sleep(60)



def main():
    global max_bottom
    global max_left
    global max_top
    global max_right
    global CONN

    reload_token(None, None)
    token = get_token()
    CONN = get_conn(token)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGHUP, reload_token)
    stream = MyStreamer(token['consumer_key'], token['consumer_secret'], token['access_token'], token['access_token_secret'])

    
    arr = LOCATION.split(',')


    max_left = float(arr[0])
    max_bottom = float(arr[1])
    max_right = float(arr[2])
    max_top = float(arr[3])
    
    while True:
        try:
	    print "start stream"
            stream.statuses.filter(locations=LOCATION)
        except:
	    traceback.print_exc(file=sys.stdout)
            pass
    

if __name__ == "__main__":
    main()


