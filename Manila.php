<?php

    $dbfile = "/home/geotttsh/geotalk/geotalk_Manila";
    $geo = null;
    $left_max = null;
    $right_max = null;
    $top_max = null;
    $bottom_max = null;
    $sql = null;
    $day = null;
    $default = 1 * 24 * 3600;
    $now = null;

    if (isset($_GET["twitter"]))
    {
	$twitter = $_GET["twitter"];

	if ($twitter == "list")
	{
	    $keys = array();
	    foreach (range(50, 0) as $number) 
	    {
		array_push($keys, "Manila_talk_".$number);
	    }

	    $memcache_obj = memcache_connect('127.0.0.1', 11211);
	    $var = memcache_get($memcache_obj, $keys);
            $arr = array();


	    header('Content-Type: application/json');

	    foreach($var as $key=>$value)
		array_push($arr, json_decode($value));

	    print json_encode($arr);

	}
	return;
    }

    if (isset($_GET["geo"]))
    {
	$geo = explode(",", $_GET["geo"]);

	if (sizeof($geo) == 4)
	{
	    $bottom_max = $geo[0];
	    $left_max = $geo[1];
	    $top_max = $geo[2];
	    $right_max = $geo[3];
	}

        $sql = "SELECT COUNT(DISTINCT uid) as count FROM user WHERE lat <= ".$top_max." AND lat >= ".$bottom_max." AND lng <= ".$right_max." AND lng >= ".$left_max;

        if (isset($_GET["recent"]))
	{
	    $now = time();
	    $day = $_GET["recent"];

	    if ($day != "")
	    {
                $sql .= " AND time >= ".intval($now - (floatval($day) * $default));
	    }
	}

        $db = new SQLite3($dbfile);

        $results = $db->query($sql);

        while($row = $results->fetchArray())
        {
            print $row["count"];
	    break;
        }

        $db->close();

	return;
    }

?>
