<?php

    $dbfile = "%DBFILE%";
    $geo = null;
    $left_max = null;
    $right_max = null;
    $top_max = null;
    $bottom_max = null;
    $sql = null;
    $day = null;
    $default = 1 * 24 * 3600;
    $now = null;
    $location = "%LOCATION%";
    $city = "%CITY%";
    $cityarr = array(%CITYARR%);
    $buymessagebutton10 = "%BUYMESSAGE10%";
    /**
    $button_array = array(
	"en" => array(%en%), 
	"ja" => array(%ja%), 
	"fr" => array(%fr%), 
	"ko" => array(%ko%), 
	"de" => array(%de%), 
	"zh-TW" => array(%zh-TW%), 
	"zh-CN" => array(%zh-CN%), 
	"pt" => array(%pt%), 
	"it" => array(%it%), 
	"es" => array(%es%), 
	"el" => array(%el%), 
	"ru" => array(%ru%), 
	"ar" => array(%ar%), 
	"th" => array(%th%), 
    );
    **/

    if (isset($_GET["twitter"]))
    {
	$twitter = $_GET["twitter"];

	if ($twitter == "list")
	{
	    $keys = array();
	    foreach (range(50, 0) as $number) 
	    {
		array_push($keys, $city."_talk_".$number);
	    }

	    $memcache_obj = memcache_connect('127.0.0.1', 11211);
	    $var = memcache_get($memcache_obj, $keys);
            $arr = array();


	    header('Content-Type: application/json; charset=utf-8');

	    foreach($var as $key=>$value)
		array_push($arr, json_decode($value));

	    print json_encode($arr);

	}
	else
	{
	    if ($twitter == "hot")
	    {
	        $memcache_obj = memcache_connect('127.0.0.1', 11211);
	        $var = memcache_get($memcache_obj, $city."_hot");

	        header('Content-Type: application/json; charset=utf-8');
	        print $var;
	    
	    }

	    if ($twitter == "home")
	    {
	        $keys = array();
		foreach ($cityarr as $city) 
		{
		    array_push($keys, $city."_talk_50");
		}

	        $memcache_obj = memcache_connect('127.0.0.1', 11211);
	        $var = memcache_get($memcache_obj, $keys);
                $arr = array();

	        header('Content-Type: application/json; charset=utf-8');

	        foreach($var as $key=>$value)
		    array_push($arr, json_decode($value));

	        print json_encode($arr);

		#print_r($keys);
	    }
	}

	return;
    }

    if (isset($_GET["geo"]))
    {
	$day = "";
	$geo = explode(",", str_replace("'", '', $_GET["geo"]));
	$now = time();

	if (sizeof($geo) == 4)
	{
	    $bottom_max = $geo[0];
	    $left_max = $geo[1];
	    $top_max = $geo[2];
	    $right_max = $geo[3];
	}

        $sql = "SELECT COUNT(DISTINCT uid) as count FROM user WHERE lat <= ".$top_max." AND lat >= ".$bottom_max." AND lng <= ".$right_max." AND lng >= ".$left_max;
        $talk_sql = "SELECT `uid`, `name`, `rname`, `image`, `media`, `create`, `lat`, `lng`, `text` FROM user WHERE lat <= ".$top_max." AND lat >= ".$bottom_max." AND lng <= ".$right_max." AND lng >= ".$left_max;

        if (isset($_GET["recent"]))
	{
	    $day = str_replace("'", '', $_GET["recent"]);

	    if ($day != "" && $day != '0')
	    {
		$tmp = intval($now - (floatval($day) * $default));
                $sql .= " AND time >= ".$tmp;
                $talk_sql .= " AND time >= ".$tmp;
	    }
	}

	header('Content-Type: application/json; charset=utf-8');
	$mysqli = new mysqli("mysql.geottt.com", "geotttdb", "1q!2w@3e#", "gttt_".strtolower($city));

        if (mysqli_connect_errno()) 
        {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }

        if (isset($_GET["talking"]))
	{
            $arr = array();
            $hashtable = array();

	    try {

	        $results = $mysqli->query($talk_sql." ORDER BY time DESC LIMIT 50");

                while($row = $results->fetch_array(MYSQLI_ASSOC))
	        {
	            if (array_key_exists($row['uid'], $hashtable)) 
		    {
		        continue; 
		    }

		    $hashtable[$row['uid']] = "1";

	            array_push($arr, array("name" => $row['name'], "rname" => $row['rname'], "image" => $row['image'], "media" => $row['media'], "text" => $row['text'], "created_at" => $row['create'], "lat" => $row['lat'], "lng" => $row['lng']));
	        }

	    } catch (Exception $e) {

	    }

	    print json_encode($arr);
	}
	else
	{

	    $results = $mysqli->query($sql);

            while($row = $results->fetch_array(MYSQLI_ASSOC))
            {
	        $key = uniqid('trans_');
	        $count = $row["count"];
	        $price = (int)$count/10;

	        $memcache_obj = memcache_connect('127.0.0.1', 11211);
	        memcache_set($memcache_obj, $key, '{"count": "'.$row["count"].'", "price": "'.$price.'", "geo": "'.$geo.'", "city": "'.$city.'", "recent": "'.$day.'", "time": "'.$now.'", "domain": "'.$_SERVER['SERVER_NAME'].'"}');

                print '{"count": "'.$count.'", "id": "'.$buymessagebutton10.'", "price": "'.$price.'", "trans": "'.$key.'"}';
	        break;
            }

	}

	$results->free();
	$mysqli->close();

	return;
    }

?>
